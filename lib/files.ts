import fs from 'fs';
import path from 'path';
import matter, { GrayMatterFile } from 'gray-matter';
import remark from 'remark';
import html from 'remark-html';
//import path from 'path'


const postsDirectory = path.resolve(process.cwd(), 'content', 'posts');
const pagesDirectory = path.resolve(process.cwd(), 'content', 'pages');

// Get all filenames in posts directory as ['en/filename.md']
export function getAllFileNames(directoryPath: string, filesList = []) {
  const files = fs.readdirSync(directoryPath);

  files.forEach((file) => {
    if (fs.statSync(`${directoryPath}/${file}`).isDirectory()) {
      filesList = getAllFileNames(`${directoryPath}/${file}`, filesList);
    } else {
      filesList.push(path.join(path.basename(directoryPath), '/', file));
    }
  });

  const filteredListmd = filesList.filter((file) => file.includes('.md'));
  const filteredListjson = filesList.filter((file) => file.includes('.json'));
  const filteredList=[...filteredListmd,...filteredListjson];
  return filteredList;
}

// Sorts posts by date
export function getSortedPostData() {
  const fileNames = getAllFileNames(postsDirectory);
//console.log(fileNames[0]);
  const allPostsData = fileNames.map((fileName) => {
    const id = path.basename(fileName,'.md');//.split('\\')[0].replace(/\.md$/, '');
    const fullPath = path.join(postsDirectory, fileName);
    const fileContents = fs.readFileSync(fullPath, 'utf-8');
    const frontMatter: GrayMatterFile<string> = matter(fileContents);

    return {
      id,
      ...(frontMatter.data as {
        lang: string;
        date: string;
        category: string;
      }),
    };
  });
 // console.log(allPostsData);
  return allPostsData.sort((a, b) => {
    if (a.date < b.date) {
      return 1;
    } else {
      return -1;
    }
  });
}

// Get IDs for posts
export function getAllIds(type = 'post') {
  const dir = type === 'page' ? pagesDirectory : postsDirectory;
  //const fileNames = getAllFileNames(dir);
  const fileNamesPages = getAllFileNames(pagesDirectory);
  const fileNamesPost = getAllFileNames(postsDirectory);
  const pages=fileNamesPages.map((fileName) => ({
    params: {
      id: path.basename(fileName,'.md'),//path.resolve(fileName).split('/')[1].replace(/\.md$/, ''),
      //lang: fileName.split(path.sep)[0],//path.resolve(fileName).split('\')[0],
      posttype:'page'
    },locale: fileName.split(path.sep)[0],
    
  }));
  const posts=fileNamesPost.map((fileName) => ({
    params: {
      id: path.basename(fileName,'.md'),//path.resolve(fileName).split('/')[1].replace(/\.md$/, ''),
      //lang: fileName.split(path.sep)[0],//path.resolve(fileName).split('\')[0],
      posttype:'post',
    },locale: fileName.split(path.sep)[0],
  }));
  const fileNames=[...posts,...pages];
  return(fileNames);
 // console.log (fileNames)
//console.log(fileNames[0].split('/')[1].replace(/\.md$/, ''));
//fileNames.map((fileName) => ({console.log(fileName)}));
 /* return fileNames.map((fileName) => ({
    params: {
      id: path.basename(fileName,'.md'),//path.resolve(fileName).split('/')[1].replace(/\.md$/, ''),
      //lang: fileName.split(path.sep)[0],//path.resolve(fileName).split('\')[0],
      
    },locale: fileName.split(path.sep)[0],
  })); */
}

export async function getContentData(id: string, type = 'post') {
  const dir = type === 'page' ? pagesDirectory : postsDirectory;
  //console.log(`fk cdata ${dir}`)
  const fullPath = path.join(dir, `${id}.md`);
  const fileContents = fs.readFileSync(fullPath, 'utf-8');
  const frontMatter = matter(fileContents);

  const processedContent = await remark()
    .use(html)
    .process(frontMatter.content);

  const contentHtml = processedContent.toString();

  return {
    id,
    ...(frontMatter.data as { date: string; title: string }),
    contentHtml,
  };
}

export function getNamespaces() {
  const fileNames = getAllFileNames(pagesDirectory);
  //console.log(fileNames);
  const filteredList = fileNames.filter((file) => file.includes('.json'));
  //console.log(filteredList);
  
  return filteredList.map((fileName) => ({
    params: {
      id: path.basename(fileName),//path.resolve(fileName).split('/')[1].replace(/\.md$/, ''),
      lang: fileName.split(path.sep)[0],//path.resolve(fileName).split('\')[0],
      locale: fileName.split(path.sep)[0],
    },
  })); 
}