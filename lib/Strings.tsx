import { getNamespaces,getSortedPostData } from './files'; 
//let f=getNamespaces();
//const allNamespaces=getNamespaces();
//const allPostsData = getSortedPostData();
export const LangStrings={
  en: {
    about: 'About',
    articles: 'Articles',
    home: 'Home',
    slogan: 'I know that I know nothing',
    blog_name:'Den Soq Blog',
  },
  ru: {
    about: 'Обо мне',
    articles: 'Статьи',
    home: 'Домой',
    slogan: 'Я знаю, что ничего не знаю',
    blog_name:'Блог Дэна Сока',
  },
} ; 
/*export default  function LangStrings(){ 
  
 //console.log(f);
  const str={
    en: {
      about: 'About',
      articles: 'Articles',
      home: 'Home',
      slogan: 'I know that I know nothing',
      blog_name:'Den Soq Blog',
    },
    ru: {
      about: 'Обо мне',
      articles: 'Статьи',
      home: 'Домой',
      slogan: 'Я знаю, что ничего не знаю',
      blog_name:'Блог Дэна Сока',
    },
  }  

  return(str)};
  */
