import { useContext } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Navigation from '../Navigation';
import Logo from '../Logo';
import { LanguageContext, locales } from '../../lib/LanguageProvider';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
interface Props {
  className?: string;
  children?: React.ReactNode;
}

const Header: React.FC<Props> = ({ className, children }) => {
  const headerClass = className || 'header';
  //const [locale, setLocale] = useContext(LanguageContext);
  //const { t } = useTranslation('common');
  const router = useRouter();
/*
  function handleLocaleChange(language: string) {
    if (!window) {
      return;
    }

    const regex = new RegExp(`^/(${locales.join('|')})`);
    localStorage.setItem('lang', language);
    setLocale(language);

    router.push(router.pathname, router.asPath.replace(regex, `/${language}`));
  }
*/
  return (
    <header className={headerClass}>
      <Logo link={`/`} />
      <Navigation />
      {children}
      <div className="lang">
        {/*
        <button onClick={() => handleLocaleChange('en')}>EN</button>
        <button onClick={() => handleLocaleChange('ru')}>Русский</button>
        */}
        <Link
            href={router.pathname}
            locale='en'
          >
        <button>EN</button>
        </Link>
        <Link
            href={router.pathname}
            locale='ru'
          >
        <button>RU</button>
        </Link>
        
      </div>
    </header>
  );
};

export default Header;
