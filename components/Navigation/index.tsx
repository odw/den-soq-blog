import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router'
import useElveTranslation from '../../lib/useTranslation';
import { useTranslation } from 'next-i18next'
interface Props {
  className?: string;
}

const Navigation: React.FC<Props> = ({ className }) => {
  const { t } = useTranslation('common');
  //const {  locale } = useElveTranslation();
  const navClass = className || 'navigation';
  const router = useRouter();
  return (
    <nav className={navClass}>
      <ul>
        <li>
          <Link href={`/`} locale={router.locale}>
            <a>{t('home')}</a>
          </Link>
        </li>
        <li>
          <Link href={`/posts`} locale={router.locale}>
            {t('articles')}
          </Link>
        </li>
        <li>
          <Link href={`/post/about`} locale={router.locale}  >
            {t('about')}
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
