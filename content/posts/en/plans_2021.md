---
lang: en
title: "Plans 2021"
slug: article
date: "2021-04-17"
category: post
description: ""
---

#### High priority

- My Blog
- My Youtube channel
- Next JS
- React
- Wordpress
- Portuguese
- Dutch
- Swedish


#### Middle priority

- Wordpress
- AWS list on Youtube
- Python
- Terraform
- Jenkins
- Norwegian
- Spanish
- French


#### Low priority

- Youtube video full css
- Sass
- Read w3c css 
- Japanese
- Chinese
- Polish

#### Very low priority

- ML Deepspeech
- Medicine vertebra
- Medicine anatomy
- Medicine viruses
- Cryptography and sec

