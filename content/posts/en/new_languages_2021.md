---
lang: en
title: "What the best ways of learning new language for free in 2021 by Den Soq."
slug: article
date: "2021-05-08"
category: post
description: ""
---

Hello my name is Den Soq.I am an IT Engineer with a passion for learning languages. I have been learning languages since the beginning of school. For now I am completely satisfied with my level of English and now focusing on French,Dutch (Netherland), Norwegian, Swedish and Spanish.
I share with you my way of learning foreign languages and my advice for you to find your best way of learning.
For me there are 2 ways of learning:First is fast and easy but may be costly and Second is free but may be long and hard.

### 1st way: pay to win.
Find a teacher or take group lessons. If you have spare money to invest in yourself please do so. Accept that you can't learn a new language in 3 to 6 month. Be real. It may take 3-5 years to get to intermediate level.

### 2nd: free way.
1. News app.Install Google Translator app. Translate word news to the language you want to learn. Copy it to clipboard.Open App store or Google Market .Paste to search from clipboard . Install a news app to your phone. .Read news articles when you can.Translate some frequently repeated words. Good for learning new words.Good for busy or lazy people.The Notifications rules!!!
2. News channel on Youtube. Install Google Translator app. Translate word news to the language you want to learn. Copy it to clipboard. Open Youtube app. Paste to search from clipboard. Subscribe. Watch when you can. It may have no subtitles. Good listening practice.Good for busy or lazy people.There are always new news.
3. Youtube videos in native language with subtitles. Translate subtitles to the language you want to learn. On phones subtitles may be available only in browsers. Speech recognition quality may be medium. Good for learning new words. Good for those who spend much time on Youtube.
4. Youtube videos in language you are interested in with subtitles. Choose the topic of your desired content. Translate topic in translator.Search topic in youtube. If subtitles are available then turn on translation to your native language or if you want to learn new words turn on subtitles in original language.On phones subtitles may be available only in browsers. Speech recognition quality may be medium. Good for listening practice.Good for learning new words. Good for those who spend much time on Youtube
5. Listen to an online radio station. Open App store or Google Market . In search type for example for french Radio France. Install app. Listen. It can be news or music. Make sure songs are in the language you want to learn. Experiment with stations and choose what you like. Try to find a radio station app in the app store or google market and install. It may take to the new level of learning. App may show you the names of the songs playing right now. Then you can find lyrics of songs on the internet. Translate lyrics to get new words.Good listening practice.Good for learning new words. Good for music lovers.
6. Wikipedia. It may have a translation of an article you are reading in a language you are interested in. Be curious. Open article in desired language in the new tab and read.Good for learning new words. Good for scholars,students and scientists.
7. Games and Apps.Some apps and games on your device may have translation to a language you are interested in. Good for games addicts and those who work a lot in computer programs.
8. Operating System Language of your device. Change to a language you are interested in. I don't recommend using this method on a beginner level.Good level up for intermediate level.

