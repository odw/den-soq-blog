---
lang: ru
title: "Планы 2021"
slug: article
date: "2021-04-17"
category: post
description: ""
---

#### Высокий приоритет

- Мой Блог
- Мой Youtube канал
- Next JS
- React
- Wordpress
- Португальский
- Голландский
- Шведский


#### Средний приоритет

- Wordpress
- AWS список на Youtube
- Python
- Terraform
- Jenkins
- Норвежский
- Испанский
- Французский


#### Низкий приоритет

- Youtube видео full css
- Sass
- Читать w3c css 
- Японский
- Китайский
- Польский

#### Очень низкий приоритет

- ML Deepspeech
- Медицина позвоночник
- Медицина анатомия
- Медицина вирусы
- Криптография и безопасность

