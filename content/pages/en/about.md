---
lang: en
title: 'About'
---

Hello my name is Den Soq.I am an IT Engineer with a passion for learning languages. I started to learn IT in 1993,when I was 15 years old. Working as a System Administrator since 2000.From 2013 to 2017 I studied Android programming. In 2017 I started learning web development. I have been learning languages since the beginning of school. My priorities changed sometimes, but I never stopped learning some languages.  For now I am completely satisfied with my level of English and now focusing on Portuguese, Norwegian, Dutch (Netherland), Swedish and  French. Here I share with you my way of learning IT, languages and other topics, give my advice with tips and tricks of the learning process,share my current progress and plans. Now a little disclaimer. Here I don't try to teach you something.Here I will share with you how I’m learning topics that I’m interested in. Mostly it's IT and languages. Hope my content will inspire you to learn IT or a new language or progress on current learning. 
