import { useState } from 'react';
import { NextPage, GetStaticProps, GetStaticPaths } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router'
import Layout from '../components/Layout';
import { getSortedPostData,getNamespaces } from '../lib/files';
//import useTranslation from '../lib/useTranslation';
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
//import { locales } from '../lib/LanguageProvider';
import { Router } from 'next/router';

interface Props {
  locale: string;
  allPostsData: {
    date: string;
    title: string;
    lang: string;
    description: string;
    id: any;
  }[];
}

const Post: NextPage<Props> = ({ locale, allPostsData }) => {
  const { t } = useTranslation('common');
  const postsData = allPostsData.filter((post) => post.lang === locale);
  const router = useRouter();
//console.log(`fk7 ${router.locale}`)
  // Pagination
  const postsPerPage = 10;
  const numPages = Math.ceil(postsData.length / postsPerPage);
  const [currentPage, setCurrentPage] = useState(1);
  const pagedPosts = postsData.slice(
    (currentPage - 1) * postsPerPage,
    currentPage * postsPerPage
  );
  //console.log()
  const loc=locale
  {/*href={`/[lang]/post/[id]`} as={`/${post.lang}/post/${post.id}`} >*/}
  return (
    <Layout className="posts" title={t('articles')}>
      <section className="page-content">
        <h1>{t('articles')} </h1>
        {pagedPosts.map((post) => (
          <article key={post.id} className="post"> 
            <Link href={`/${router.locale}/post/[id]`} as={`/${router.locale}/post/${post.id}`}  > 
              <a>
                <h3>{post.title}</h3> 
              </a>
            </Link>
            <time>
              {new Date(post.date).toLocaleDateString(locale, {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
              })}
            </time>
            {/*post.description && <p>{post.description}</p>*/}
          </article>
        ))}

        {numPages > 1 && (
          <div className="pagination">
            {Array.from({ length: numPages }, (_, i) => (
              <button
                key={`pagination-number${i + 1}`}
                onClick={() => setCurrentPage(i + 1)}
                className={currentPage === i + 1 ? 'active' : ''}
              >
                {i + 1}
              </button>
            ))}
          </div>
        )}
      </section>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async ({locale}) => {
  const allPostsData = getSortedPostData();
  //const allNamespaces=getNamespaces();
  //console.log(locale);
  return {
    props: {
      //locale: ctx.params?.lang || 'ru',
      ...await serverSideTranslations(locale, ['common',]),
      locale:locale,
      allPostsData,
    },
  };
};

/*
export const getStaticProps = async ({ locale }) => {
  const allPostsData = getSortedPostData();
 return {
   props: {
    ...await serverSideTranslations(locale, ['common',]),allPostsData,
  }
}};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    //paths: [{ params: { lang: 'en' } }, { params: { lang: 'ru' } }],
    paths: [{ params: { locale: 'en' } }, { params: { locale: 'ru' } }],
    fallback: false,
  };
};*/
/*
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['common']),
  },
})
*/
//export default Home
export default Post;


